using NUnit.Framework;
using ShipsFight;

namespace ShootTesting
{
    [TestFixture]
    public class Tests
    {

        private Field _field;

        [SetUp]
        public void Setup()
        {
            _field = new Field(10);
        }

        
        [TestCase("OneDecks", 1, 1)]
        [TestCase("TwoDecks", 1, 1)]
        [TestCase("ThreeDecks", 1, 1)]
        [TestCase("FourDecks", 1, 1)]
        public void TestShootHorisontal(string type, int row, int col)
        {
            Ship ship = ShipBuilder.Get(type);
            Position pos = new Position(row, col, true);

            ship.PlaceShip(pos);
            _field.PlaceShip(ship);
            _field.Shoot(2, 2);
            _field.Shoot(2, 3);
            _field.Shoot(2, 4);
            _field.Shoot(2, 5);
            foreach (Ship shipTest in this._field.ships)
            {
                Assert.IsTrue(shipTest.IsKilled);
            }
               
        }

        [TestCase("OneDecks", 1, 1)]
        [TestCase("TwoDecks", 1, 1)]
        [TestCase("ThreeDecks", 1, 1)]
        [TestCase("FourDecks", 1, 1)]
        public void TestShootVertical(string type, int row, int col)
        {
            Ship ship = ShipBuilder.Get(type);
            Position pos = new Position(row, col, false);

            ship.PlaceShip(pos);
            _field.PlaceShip(ship);
            _field.Shoot(2, 2);
            _field.Shoot(3, 2);
            _field.Shoot(4, 2);
            _field.Shoot(5, 2);
            foreach (Ship shipTest in this._field.ships)
            {
                Assert.IsTrue(shipTest.IsKilled);
            }

        }
    }
}