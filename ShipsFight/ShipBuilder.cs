﻿using ShipsFight.ships;
using System;
using System.Collections.Generic;
using System.Text;

namespace ShipsFight
{
    public class ShipBuilder
    {

        private static int ShipId = 0;

        public static Ship Get(string shipType)
        {
            ShipId = ShipId + 1;
            switch (shipType)
            {
                case "FourDecks":
                    return new FourDecks(ShipId);
                case "ThreeDecks":
                    return new ThreeDecks(ShipId);
                case "TwoDecks":
                    return new TwoDecks(ShipId);
                case "OneDecks":
                    return new OneDecks(ShipId);
                default:
                    throw new InvalidOperationException("Неправильный тип корабля");
            }
        }
    }
}
