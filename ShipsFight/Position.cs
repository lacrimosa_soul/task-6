﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShipsFight
{
    public class Position
    {
        private int Row;
        private int Column;
        private bool Horisontal;

        public Position(int row, int column, bool horisontal)
        {
            this.Row = row;
            this.Column = column;
            this.Horisontal = horisontal;
        }

        public Position(int row, int column)
        {
            this.Row = row;
            this.Column = column;
        }

        public int GetRow()
        {
            return Row;
        }

        public int GetColumn()
        {
            return Column;
        }

        public Position GetEndPosition(int size)
        {
            if (Horisontal.Equals(false))
            {
                return new Position(Row + size, Column);
            }
            else
            {
                return new Position(Row, Column + size);
            }
        }


        public bool GetHorizontal()
        {
            return Horisontal;
        }
    }
}
