﻿using ShipsFight.ships;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace ShipsFight
{
    public class Field
    {
        private string[,] area;
        private Random random = new Random();
        private int dimension;
        private readonly Dictionary<Type, int> shipsType;
        public IList<Ship> ships = new List<Ship>();

        public Field(int dimension)
        {
            this.dimension = dimension;
            area = new string[dimension, dimension];
            this.shipsType = new Dictionary<Type, int>
            {
                { typeof(FourDecks), 1 },
                { typeof(ThreeDecks), 2 },
                { typeof(TwoDecks), 3 },
                { typeof(OneDecks), 4 }
            };
        }

        private Position GetRandomShipPosition(int shipSize, bool horizontal)
        {
            int row;
            int col;

            if (horizontal)
            {
                row = random.Next(0, dimension - 1);
                col = random.Next(0, dimension - 1 - shipSize);
            }
            else
            {                
                row = random.Next(0, dimension - 1 - shipSize);
                col = random.Next(0, dimension - 1);
            }
            return new Position(row, col, horizontal);
        }


        private Ship FindShip(int id)
        {

            foreach (Ship addedShip in this.ships)
            {
                if (addedShip.Id == id)
                {
                    return addedShip;
                }
            }

            return null;
        }

        public void AddShips()
        {
            foreach (var shipType in this.shipsType)
            {
                for (int i = 0; i < shipType.Value; i++)
                {
                    bool horizontal = this.GetRandomDirection();
                    Ship ship = ShipBuilder.Get(shipType.Key.Name);

                    ship.PlaceShip(this.GetRandomShipPosition(ship.Size, horizontal));
                    while (!this.CanBePlaced(ship))
                    {
                        ship.PlaceShip(this.GetRandomShipPosition(ship.Size, horizontal));
                    }                    
                    PlaceShip(ship);
                }
            }
        }

        public void PlaceShip(Ship ship)
        {
            int shipRow = ship.GetStartPosition().GetRow();
            int shipCol = ship.GetStartPosition().GetColumn();
            int endRow = ship.GetEndPosition().GetRow();
            int endCol = ship.GetEndPosition().GetColumn();

            if (shipRow == endRow)
            {
                for (int j = shipCol; j < endCol; j++)
                {
                    area[shipRow, j] = ship.Id.ToString();
                }
            }
            else
            {
                for (int j = shipRow; j < endRow; j++)
                {
                    area[j, shipCol] = ship.Id.ToString();
                }
            }
            ships.Add(ship);
        }

        private bool CanBePlaced(Ship ship)
        {
            int shipRow = ship.GetStartPosition().GetRow();
            int shipCol = ship.GetStartPosition().GetColumn();
            int endRow = ship.GetEndPosition().GetRow();
            int endCol = ship.GetEndPosition().GetColumn();

            if (shipRow == endRow)
            {
                for (int j = shipCol; j < endCol; j++)
                {
                    if (area[shipRow, j] != null)
                    {
                        return false;
                    }
                }
            }
            else
            {
                for (int j = shipRow; j < endRow; j++)
                {
                    if (area[j, shipCol] != null)
                    {
                        return false;
                    }
                }
            }
            return true;

        }


        private bool GetRandomDirection()
        {
            return random.Next(0, 2) == 0 ? false : true;
        }

        public void Shoot(int row, int col)
        {            
            string id = area[row - 1, col - 1];
            if (id != null)
            {
                int result = Int32.Parse(id);
                Ship ship = FindShip(result);
                ship.HitShip();
                area[row - 1, col - 1] = "X";
            }
            else
            {
                Console.WriteLine("Промах!");
            }
        }

        public void PrintField()
        {
            Console.WriteLine("Поле боя:");
            for (int i = 0; i < area.GetLength(0); i++)
            {
                for (int j = 0; j < area.GetLength(1); j++)
                {
                    string res = area[i, j];
                    if (res == null)
                    {
                        res = "0";
                    }
                    Console.Write(res + " ");
                }
                Console.WriteLine();
            }
        }

        public void PrintStatistic()
        {
            Console.WriteLine("Статистика:");
            foreach (Ship ship in this.ships)
            {
                var json = JsonSerializer.Serialize(ship);
                byte[] jsonUtf8Bytes;
                var options = new JsonSerializerOptions
                {
                    WriteIndented = true
                };
                jsonUtf8Bytes = JsonSerializer.SerializeToUtf8Bytes(ship, options);
                string result = System.Text.Encoding.UTF8.GetString(jsonUtf8Bytes);

                Console.WriteLine(result);
            }
        }

    }
}
