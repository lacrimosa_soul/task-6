﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShipsFight.ships
{
    public class ThreeDecks : Ship
    {
        public ThreeDecks(int id) : base(id, "ThreeDecks", 3)
        {

        }
    }
}
