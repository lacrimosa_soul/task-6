﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShipsFight.ships
{
    class TwoDecks : Ship
    {
        public TwoDecks(int id) : base(id, "TwoDecks", 2)
        {

        }
    }
}
