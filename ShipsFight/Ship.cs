﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace ShipsFight
{
    public class Ship
    {
        public int Id;

        public int Size { get; set; }

        public string Type { get; set; }

        public int Damage { get; set; }
        public Boolean IsKilled { get; set; }
        private Position Position;

        public Ship(int id, string type, int size)
        {
            this.Size = size;
            this.Id = id;
            this.Type = type;
        }

        public void PlaceShip(Position position)
        {
            this.Position = position;
        }


        public void HitShip()
        {

            Damage += 1;
            if (Damage >= Size)
            {
                HitKill();
            }
            else
            {
                HitAlert();
            }
        }

        private void HitAlert()
        {
            Console.WriteLine("Попадание в корабль {0}", Type);
        }

        private void HitKill()
        {
            if (IsKilled)
            {
                Console.WriteLine("Вы попали в уже потопленый корабль типа {0}", Type);
            } else
            {
                Console.WriteLine("Корабль {0} потоплен", Type);
                IsKilled = true;
            }
            
        }

        public Position GetStartPosition()
        {
            return Position;
        }

        public Position GetEndPosition()
        {
            return Position.GetEndPosition(Size);
        }
    }
}
