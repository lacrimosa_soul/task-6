﻿using System;

namespace ShipsFight
{
    class Program
    {
        static int dimension = 10;

        static void Main(string[] args)
        {
            Console.WriteLine("Добро пожаловать в морской бой");
            Field field = new Field(dimension);            
            field.AddShips();
            Console.WriteLine("Корабли размещены");            
            Boolean exit = false;
            DisplayMenu();
            while (!exit)
            {
                Console.WriteLine("Введите команду:");
                string command = Console.ReadLine();
                switch (command)
                {
                    case "fire":
                        int row = GetCoord("Row");
                        int col = GetCoord("Col");
                        field.Shoot(row, col);
                        break;
                    case "exit":
                        field.PrintField();
                        exit = true;
                        break;
                    case "stat":
                        field.PrintStatistic();
                        break;
                    default:
                        Console.WriteLine("Неверная команда!");
                        DisplayMenu();
                        break;
                }
            }

            Console.WriteLine("Работа завершена, нажмите любую клавишу:");
            Console.ReadKey();
        }

        private static void DisplayMenu()
        {
            Console.WriteLine("Введите координаты для выстрела или exit для завершения игры");
            Console.WriteLine("Список команд:");
            Console.WriteLine("fire - сделать выстрел");
            Console.WriteLine("stat - вывести статистику");
            Console.WriteLine("exit - завершение работы");
        }

        private static int GetCoord(string outputText)
        {
            int parse;
            Console.WriteLine(outputText);
            string tempInput = Console.ReadLine();
            if (!int.TryParse(tempInput, out parse))
            {
                Console.WriteLine("Неверное значение! Введите число:");
                return GetCoord(outputText);
            }
            if (parse == 0)
            {
                Console.WriteLine("Значение должно быть больше 1");
                return GetCoord(outputText);
            }
            if (parse > dimension)
            {
                Console.WriteLine("Значение должно быть меньше размера поля {0}", dimension);
                return GetCoord(outputText);
            }
            return parse;
        }
    }
}
